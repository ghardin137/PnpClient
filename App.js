import React from 'react';
import { StyleSheet, Text, View, KeyboardAvoidingView } from 'react-native';
import { ApolloProvider } from 'react-apollo';
import configureClient from "./graphql/configure-client";
import MessageList from "./components/MessageList";
import MessageInput from "./components/MessageInput";

export default class App extends React.Component {
  client: any;
  constructor() {
    super();
    this.client = configureClient();
  }
  render() {
    return (
      <ApolloProvider client={this.client}>
        <KeyboardAvoidingView style={styles.container} behavior="height">
          <MessageList />
          <MessageInput />
        </KeyboardAvoidingView>
      </ApolloProvider>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    paddingTop: 20,
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
});
