import React from "react";
import { StyleSheet, Text, View } from 'react-native';

export default class Message extends React.Component {
    render() {
        return (
            <View style={styles.container}>
                <Text>{this.props.item.message}</Text>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
      flexDirection: "row",
      backgroundColor: '#fff',
      padding: 10,
      borderBottomWidth: 1,
      borderStyle: "solid",
      borderBottomColor: "#303030",
      alignItems: 'center',
      justifyContent: 'flex-start',
    },
});