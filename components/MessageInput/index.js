import React from "react";
import { StyleSheet, Text, View, TextInput, TouchableHighlight, KeyboardAvoidingView } from 'react-native';
import gql from "graphql-tag";
import { graphql } from "react-apollo";

class MessageInput extends React.Component {
    state = {
        message: "",
    }

    onMessageChange = (value) => {
        this.setState({ message: value });
    }

    handleSave: Function = () => {
        if(this.state.message !== "") {
            this.props.mutate({
                variables: {
                    input: {
                        gameId: "e498ec51-07c0-4bdb-922d-62a27e3127c8",
                        message: this.state.message,
                        playerFromId: "afc69469-e085-40cc-b056-055868318f33",
                        action: false,
                    }
                }
            }).catch((ex) => {
                console.log(ex);
            });
        }
    }
    render() {
        return (
            <KeyboardAvoidingView style={styles.container}>
                <TextInput
                    style={{
                        paddingTop: 9,
                        paddingBottom: 9,
                        paddingLeft: 10,
                        marginTop: 10,
                        borderWidth: 1,
                        borderRadius: 10,
                        borderStyle: "solid",
                        borderColor: "#333",
                        flexGrow: 1,
                    }}
                    value={this.state.message}
                    onChangeText={this.onMessageChange}
                    maxLength={255}
                    editable={true}
                    selectTextOnFocus={true}
                />
                <View
                    style={{
                        flexGrow: 0,
                        marginLeft: 10,
                        borderWidth: 1,
                        borderRadius: 10,
                        borderStyle: "solid",
                        borderColor: "#007bff",
                        backgroundColor: "#007bff",
                        opacity: 1,
                    }}
                >
                    <TouchableHighlight
                        style={{ padding: 12.5, borderRadius: 10 }}
                        underlayColor="#0069d9"
                        onPress={this.handleSave}
                    >
                        <Text
                            style={{
                                textAlign: "center",
                                color: "#fff",
                            }}
                        >
                            Send
                        </Text>
                    </TouchableHighlight>
                </View>
            </KeyboardAvoidingView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
      flexDirection: "row",
      backgroundColor: '#fff',
      padding: 10,
      marginTop: 20,
      borderTopWidth: 1,
      borderStyle: "solid",
      borderTopColor: "#303030",
      alignItems: 'center',
      justifyContent: 'flex-start',
    },
});

const sendMessage = gql`
    mutation sendMessage($input: NewMessage!) {
        sendGameMessage(messageDetails:$input){
            id
            message
            insertedAt
        }
    }
`;

const withSendMessage = graphql(sendMessage);

export default withSendMessage(MessageInput);