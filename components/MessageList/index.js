import React from "react";
import { FlatList, StyleSheet, Text, View } from "react-native";
import gql from "graphql-tag";
import Message from "../Message";
import { graphql } from "react-apollo";

class MessageList extends React.Component {

    constructor(props) {
        super(props);
        props.subscribeToNewMessages();
    }

    _renderItem = (item) => {
        return (
            <Message {...item} />
        )
    }

    _keyExtractor = (item, index) => item.id;

    render() {
        return (
            <View style={styles.container}>
                {!this.props.game.loading && this.props.game.game ?
                    <FlatList
                        data={this.props.game.game.messages}
                        keyExtractor={this._keyExtractor}
                        renderItem={this._renderItem}
                        />
                : 
                    <Text>Loading</Text>
                }
            </View>
        );
    }
}
  
const styles = StyleSheet.create({
    container: {
      paddingTop: 20,
      flex: 1,
      backgroundColor: '#fff',
      alignItems: 'center',
      justifyContent: 'flex-start',
    },
});

const getMessages = gql`
    query game($id: ID!) {
        game(id:$id){
            id
            name
            messages {
                id
                message
                insertedAt
            }
        }
    }
`;

const subscribeToMessages = gql`
    subscription gameMessages($id: ID!) {
        newGameMessages(gameId:$id){
            id
            message
            insertedAt
        }
    }
`;

const withGraphql = graphql(getMessages, {
    name: "game",
    options: ownProps => {
        return {
            variables: {
                id: "e498ec51-07c0-4bdb-922d-62a27e3127c8"
            },
        };
    },
    props: props => {
        return {
           ...props,
            subscribeToNewMessages: params => {
                return props.game.subscribeToMore({
                    document: subscribeToMessages,
                    variables: {
                        id: "e498ec51-07c0-4bdb-922d-62a27e3127c8"
                    },
                    updateQuery: (prev, {subscriptionData}) => {
                        if (!subscriptionData.data) {
                            return prev;
                        }
                        const newFeedItem = subscriptionData.data.newGameMessages;
                        newFeedItem[Symbol('id')] = `Message:${newFeedItem["id"]}`;
                        const newMessages = [...prev.game.messages, newFeedItem];
                        const game = {...prev.game };
                        game.messages = newMessages;
                        const next = {
                            ...prev,
                            game,
                        };
                        return next;
                    }
                });
            }
        };
    },
});

export default withGraphql(MessageList);

