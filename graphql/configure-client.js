import {ApolloClient} from 'apollo-client';
import { split } from 'apollo-link';
import {HttpLink} from 'apollo-link-http';
import {InMemoryCache} from 'apollo-cache-inmemory';
import * as AbsintheSocket from "@absinthe/socket";
import {createAbsintheSocketLink} from "@absinthe/socket-apollo-link";
import {Socket as PhoenixSocket} from "phoenix";
import { getMainDefinition } from 'apollo-utilities';
import {hasSubscription} from "@jumpn/utils-graphql";
// import Config from 'react-native-config';

const configureClient = () => {
    // Create an http link:
    const httpLink = new HttpLink({
        uri: `http://192.168.1.198:4000/api` //Config.GraphQLURI
    });
    
    // Create a WebSocket link:
    const wsLink = createAbsintheSocketLink(AbsintheSocket.create(
        new PhoenixSocket(`ws://192.168.1.198:4000/socket`) //Config.GraphQLWSURI)
    ));

    // using the ability to split links, you can send data to each link
    // depending on what kind of operation is being sent
    const link = split(
        // split based on operation type
        operation => hasSubscription(operation.query),
        wsLink,
        httpLink
    );

    const fetchOptions = {
        link: link,
        cache: new InMemoryCache(),
    };

    const client = new ApolloClient(fetchOptions);
    
    return client;
};

export default configureClient;